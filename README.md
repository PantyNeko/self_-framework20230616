# 基于QFramework 的小框架分享

## 前言

- 各位好 我是猫叔 这是我基于QFramework扩展的一个小型框架 下面来分享一下使用技巧
- 原生框架的规则细节我就不再赘述 大家可以前往凉鞋老师的官方号进行了解 这里着重放在个人的使用技巧和扩展部分
- 获取方式 Gitee：https://gitee.com/PantyNeko/self_-framework20230616.git

## 原生框架简介

### Architecture

- 针对 Architecture 我通常会将它视为一个项目模块管理器的集合 使用注册的方式将当前项目所使用到的 模块 系统  和 工具 添加进内部的IOC容器中 方便管理 这里 我们尝试扩展一个子类来试一下

- ```c#
  public class QF_Game : Architecture<QF_Game>
  {
      protected override void Init()
      {
          RegisterUtility<IStorage>(new JsonStorage());
          RegisterModel<IPlayerModel>(new PlayerModel());
          RegisterSystem<IGunSystem>(new GunSystem());
      }
  }
  ```

### Utility

- 针对 Utility 其实很多人都觉得是多此一举 但是真正遇到需求时还是能够发挥很好的作用的 例如 猫叔写的存储 IStorage

- 可以看到 脚本写了两个实现 继承同一个接口 这样可以针对前期和后期进行快速替换不同的实现

- ```c#
  public interface IStorage : IUtility
  {
      void Save<T>(string path, T data) where T : class;
      bool Load<T>(string path, out T data) where T : class;
  }
  public class JsonStorage : IStorage
  {
  	bool IStorage.Load<T>(string path, out T data)
  	{
  		data = SerializeHelper.LoadJson<T>(CommonData.StoragePath + path + ".json");
  		return data != null;
  	}
  	void IStorage.Save<T>(string path, T data)
  	{
  		SerializeHelper.SaveJson(CommonData.StoragePath + path + ".json", data);
  	}
  }
  public class BinaryStorage : IStorage
  {
  	bool IStorage.Load<T>(string path, out T data)
  	{
  		data = SerializeHelper.LoadBinary<T>(CommonData.StoragePath + path + ".sav");
  		return data != null;
  	}
  	void IStorage.Save<T>(string path, T data)
  	{
  		SerializeHelper.SaveBinary(CommonData.StoragePath + path + ".sav", data);
  	}
  }
  ```

### Controller

- 针对 Controller 他在Unity中大部分情况会被描述为 MonoBehaviour 脚本对象所对应的层级 它可以提供给Mono脚本对象访问架构的能力 通常的使用方法为 继承 IController 接口并实现 但是一般Unity项目中 脚本占多数 每次继承并实现会有很多重复操作和样板代码 所以 可以扩展一个基类 来 简化

- ```c#
  public class QF_GameController : MonoBehaviour, IController
  {
      IArchitecture IBelongToArchitecture.GetArchitecture() => QF_Game.Interface;
  }
  ```

### Model

- 针对Model 我会认为他是一个同类别公共数据的集合 用来管理一个或多个需要共享的数据 可以用来做数据绑定或外部调用 猫叔在使用过程中 一般不会让Model层直接参与交互 通常使用委托事件或Command进行传递 这里我们做一个玩家Model来测试一下

- ```c#
  public interface IPlayerModel : IModel
  {
      IBindableProperty<int> HP { get; }
  }
  public class PlayerModel : AbstractModel, IPlayerModel
  {
      IBindableProperty<int> IPlayerModel.HP { get; } = new BindableProperty<int>(10);
  
      protected override void OnInit() { }
  }
  ```

### BindableProperty

- 对于这个工具 通常用来做单个数据与一些动作的绑定 如上述 PlayerModel 中的 HP 需要在变更时触发 UI 变更 例如我们创建一个UI 然后 在面板脚本中 绑定变更

- ```c#
  public class MainPanel : QF_GameController
  {
      private Text hpTex;
      private void Start()
      {
          hpTex = transform.Find("HpText").GetComponent<Text>();
  
          this.GetModel<IPlayerModel>().HP.RegisterWithInitValue(hp =>
          {
              hpTex.text = $"HP:{hp}";
          })
          .UnRegisterWhenGameObjectDestroyed(gameObject);
      }
  }
  ```

### System

- 针对 System 我会认为他是一个小型公共交互管理器 用来管理一个或几个数据与其他层级或模块的交互动作 这里简单做一个枪械系统来测试一下

- ```c#
  public interface IGunSystem : ISystem
  {
      void Shoot(Vector2 dir);
  }
  public class GunSystem : AbstractSystem, IGunSystem
  {
      protected override void OnInit() { }
  
      void IGunSystem.Shoot(Vector2 dir)
      {
          Debug.Log($"当前方向为{dir},完毕 射击");
      }
  }
  ```

### Command

- 针对 Command 我一般会将他视为多系统或多模块联合交互时的一个载体 例如射击时消耗血量 我们就可以使用这种方式来进行管理 以下是代码

#### 无参命令

- ```c#
  public class ShootCommand : AbstractCommand
  {
      protected override void OnExecute()
      {
          var hp = this.GetModel<IPlayerModel>().HP;        
          hp.Value--;
          if (hp.Value == 0) return;
          this.GetSystem<IGunSystem>().Shoot(dir);
      }
  }
  ```

#### 命令参数化（原生）

- 在使用命令的时候 很多数据我们没有办法写死 通常需要外部去传入 架构本身提供了一种构造传值的方式 例如

- ```c#
  public class ShootCommand : AbstractCommand
  {
      private readonly Vector2 dir;
  
      public ShootCommand(Vector2 dir)
      {
          this.dir = dir;
      }
      protected override void OnExecute()
      {
          var hp = this.GetModel<IPlayerModel>().HP;        
          hp.Value--;
          if (hp.Value == 0) return;
          this.GetSystem<IGunSystem>().Shoot(dir);
      }
  }
  ```

### Query

- Query 这个东西我用的不多 大体上的思路跟命令差不多 用来封装多个模块或系统的联合查询

### TypeEventSystem

- 事件系统通常用来通知或回调 我们可以将它理解成广播 通过群发的形式 通知留下标记的对象

#### 定义事件

-  在这里 我希望当血量为0时 触发 死亡 例如定义一个事件叫玩家死亡事件

- ```c#
  public struct PlayerDieEvent { }
  ```

#### 发送事件

- ```c#
  public class ShootCommand : AbstractCommand
  {
      private readonly Vector2 dir;
  
      public ShootCommand(Vector2 dir)
      {
          this.dir = dir;
      }
      protected override void OnExecute()
      {
          var hp = this.GetModel<IPlayerModel>().HP;        
          hp.Value--;
          if (hp.Value == 0)
          {
              this.SendEvent<PlayerDieEvent>();
          }
          else
          {
              this.GetSystem<IGunSystem>().Shoot(dir);
          }
      }
  }
  ```

#### 注册事件

- ```c#
  public class Player : QF_GameController
  {
      private void Start()
      {
          this.RegisterEvent<PlayerDieEvent>(e =>
          {
              Debug.Log("玩家死亡");
          })
          .UnRegisterWhenGameObjectDestroyed(gameObject);
      }
  }
  ```

## 架构扩展

### 为什么要扩展？

- 在实际开发中 每个人都会有自己的习惯和遇到各种问题 这时候 难免会需要对架构进行二次扩展与开发
- 为了减少对原架构的破坏 我使用了 接口和分布类进行设计扩展

### 扩展内容

#### CommandPool 命令池

#### ICommandContainer 命令基类容器

#### IParameterCommand 参数化命令

#### QueryPool 查询池

#### IParameterQuery 参数化查询

#### 基于ScriptableObject的可序列化模块

- AbstractConfigModelBySO

  - ```c#
    public interface IBagConfigModel : IModel
    {
        bool GetByName(string name,out BagConfig config);
    }
    [Serializable]
    public class BagConfig 
    {
        public string name;
        public Sprite sprite;
        public int stack;
    }
    [CreateAssetMenu(fileName = "New BagConfigModel",
                     menuName = "Data/SO/BagConfigModel")]
    public class BagConfigModel : AbstractConfigModelBySO, IBagConfigModel
    {
        [SerializeField] private List<BagConfig> mConfig;
    
        bool IBagConfigModel.GetByName(string name, out BagConfig config)
        {
            if(mConfig.Count > 0)
            {
                foreach (var item in mConfig)
                {
                    if(item.name == name)
                    {
                        config = item;
                        return true;
                    }
                }
            }
            config = null;
            return false;
        }
    }
    ```

- AbstractModelBySO

  - ```c#
    [Serializable]
    public class BagInfo
    {
        public int id;
        public int count;
    }
    public interface ITestModel : IModel
    {
        void AddInfo(int id, int count);
    }
    [CreateAssetMenu(fileName = "New TestModel", 
                     menuName = "Data/SO/TestModel")]
    public class TestModel : AbstractModelBySO, ITestModel
    {
        [SerializeField] private List<BagInfo> BagInfos;
        protected override void OnInit()
        {
            BagInfos = new List<BagInfo>()
            {
                new BagInfo(){id = 0,count = 1}
            };
        }
        void ITestModel.AddInfo(int id, int count)
        {
            BagInfos.Add(new BagInfo()
            {
                id = id,
                count = count
            });
        }
    }
    ```

- AbstractSystemBySO

  - ```c#
    public interface ITestSystem : ISystem
    {
        int GetNum(int index);
    }
    [CreateAssetMenu(fileName = "New TestSystem", 
                     menuName = "Data/SO/TestSystem")]
    public class TestSystem : AbstractSystemBySO, ITestSystem
    {
        [SerializeField] private List<int> nums;
        protected override void OnInit()
        {
            nums = new List<int>() { 1, 2, 3 };
        }
        int ITestSystem.GetNum(int index)
        {
            return index >= 0 && index < nums.Count ? nums[index] : -1;
        }
    }
    ```

### 附属套件

- AudioMgrSystem 音频管理系统 
- DelayTimeSystem 延时系统 
- InputDeviceMgrSystem 输入设备管理系统 
- ObjectPoolSystem 对象池系统
- SceneMgrSystem 场景管理系统
- UGUISystem 基于UGUI的管理系统
- MessageSystem 消息队列系统

### 附属工具

- ILoadConfig 配置文件加载
- IStorage 序列化存储工具