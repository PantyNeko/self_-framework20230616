﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Panty
{
    // 2的N次方    => [1 << n]
    // N*2的E次方  => [n << e]
    // N/2的E次方  => [n >> e]
    // 1=-1取反    => [~n + 1] 
    // AND 判断o数 => [(n & 1) == 0]
    // 计算以中值两侧的索引 => [i - count/2] 如果是偶数 需要 +0.5f 来补充
    // 是否在0-Max范围内 => [(uint)n < max] 如果n是一个负数，转成uint越界，变成一个很大的正数
    // 当两个最小值都为0时 
    // cur / max 将 0-max 映射到 0 - 1;
    // (max - cur) / max 将 0-max 映射到 1 - 0;

    public static class PMath
    {
        public readonly static Random random = new Random();
        /// <summary>
        /// 角度转弧度
        /// </summary>
        public const double Deg2Rad = Math.PI / 180;
        /// <summary>
        /// 弧度转角度
        /// </summary>
        public const double Rad2Deg = 180 / Math.PI;
        /// <summary>
        /// 向目标点移动
        /// </summary>
        public static void MoveTowards(this ref float cur, float target, float step)
        {
            cur = (target - cur).Abs() > step ? cur + (target - cur).Sign() * step : target;
        }
        /// <summary>
        /// 线性插值
        /// </summary>
        public static float Linear(float a, float b, float t = 0.5f) => (a + b) * t;
        /// <summary>
        /// 求绝对值[位运算优化]
        /// </summary>
        public static int Abs(this int a) => (a ^ (a >> 31)) - (a >> 31);
        /// <summary>
        /// 求绝对值 float
        /// </summary>
        public static float Abs(this float a) => a >= 0 ? a : -a;
        /// <summary>
        /// 求绝对值 double
        /// </summary>
        public static double Abs(this double a) => a >= 0 ? a : -a;
        /// <summary>
        /// 获取符号
        /// </summary>
        public static int Sign(this float f) => f >= 0f ? 1 : -1;
        public static int Sign(this int f) => f >= 0 ? 1 : -1;
        /// <summary>
        /// 大小排序 Int
        /// </summary>
        public static int Sort(this int n) => n == 0 ? 0 : n > 0 ? 1 : -1;
        /// <summary>
        /// 四舍五入取整
        /// </summary>
        public static int RoundToInt(this float n) => (int)(n >= 0 ? n + 0.5f : n - 0.5f);
        /// <summary>
        /// 将值限定在 0-1 之间
        /// </summary>
        public static void Clamp01(this ref float t) => t = t < 0f ? 0f : t > 1f ? 1f : t;
        /// <summary>
        /// 数据交换
        /// </summary>
        public static void Swap<T>(this ref T a, ref T b) where T : struct
        { T c = a; a = b; b = c; }
        public static T Swap<T>(this ref T a, T b) where T : struct
        { T c = a; a = b; return c; }
        /// <summary>
        /// 将一个范围映射到另一个范围
        /// </summary>
        public static float RangeToRange(this float cur, float min, float max, float minA, float maxA)
        {
            return minA + (maxA - minA) / (max - min) * (cur - min);
        }
        /// <summary>
        /// 将一个范围映射到另一个范围
        /// </summary>
        public static float RangeToRange(this float cur, float min, float max, float maxA)
        {
            return maxA / (max - min) * (cur - min);
        }
        /// <summary>
        /// 将一个范围映射到另一个范围[最小值都是0的情况]
        /// </summary>
        public static float RangeToRange(this float cur, float max, float maxA)
        {
            return maxA / max * cur;
        }
        /// <summary>
        /// 将最小值-最大值的范围 映射到 0 - 1
        /// </summary>
        public static float RangeTo01(this float cur, float min, float max) => cur / (max - min);
        /// <summary>
        /// 将最小值-最大值的范围 映射到 1 - 0
        /// </summary>
        public static float RangeTo10(this float cur, float min, float max) => (max - cur) / (max - min);
        /// <summary>
        /// 0-最大值的范围 映射到 1 - 0
        /// </summary>
        public static float RangeTo10(this float cur, float max) => (max - cur) / max;
        /// <summary>
        /// 检测是否在Min-Max范围内
        /// </summary>
        public static bool InRange(this float n, float min, float max) => n >= min && n <= max;
        /// <summary>
        /// 用于泛型比较
        /// </summary>
        public static bool EqualsT<T>(this T a, T b) => EqualityComparer<T>.Default.Equals(a, b);
        /// <summary>
        /// 将数组拷贝一份
        /// </summary>
        public static T[] Copy<T>(this T[] arr)
        {
            T[] c = new T[arr.Length];
            Array.Copy(arr, c, arr.Length);
            return c;
        }
        public static T[] Combine<T>(this T[] a, T[] b)
        {
            int len = a.Length;
            T[] c = new T[len + b.Length];
            Array.Copy(a, 0, c, 0, len);
            Array.Copy(b, 0, c, len, b.Length);
            return c;
        }
        /// <summary>
        /// 将BitArray转换成 整形数组
        /// </summary>
        public static int[] BitToIntArray(this BitArray array)
        {
            int[] intArray = new int[(array.Length + 31) / 32]; // 每个 int 有32位
            array.CopyTo(intArray, 0);
            return intArray;
        }
        /// <summary>
        /// 将BitArray转换成 字节数组
        /// </summary>
        public static byte[] BitToByteArray(this BitArray array)
        {
            byte[] byteArray = new byte[(array.Length + 7) / 8];
            array.CopyTo(byteArray, 0);
            return byteArray;
        }
        /// <summary>
        /// 洗牌算法打乱数组元素
        /// </summary>
        public static void Shuffle<T>(this List<T> list)
        {
            for (int i = list.Count - 1; i > 0; i--)
            {
                int j = random.Next(i + 1);
                T temp = list[i];
                list[i] = list[j];
                list[j] = temp;
            }
        }
    }
}