using QFramework;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuffPanel : QF_GameController
{
    private Dictionary<string, BuffIcon> mBuffs;

    private void Start()
    {
        mBuffs = new Dictionary<string, BuffIcon>();

        this.RegisterEvent<AddTestBuffEvent>(e =>
        {
            var obj = ResHelper.SyncLoad<GameObject>("Prefabs/BuffIcon");
            obj.transform.SetParent(transform);

            obj.GetComponent<Image>().sprite = e.config.Sprite;
            var icon = obj.GetComponent<BuffIcon>();
            icon.Init(e.buff, e.config);
            mBuffs.Add(e.buff.name, icon);
        })
        .UnRegisterWhenGameObjectDestroyed(gameObject);

        this.RegisterEvent<UpdateTestBuffEvent>(e =>
        {
            if(mBuffs.TryGetValue(e.config.Name,out var icon))
            {
                icon.UpdateInfo();
            }
        })
        .UnRegisterWhenGameObjectDestroyed(gameObject);

        this.RegisterEvent<RemoveTestBuffEvent>(e =>
        {
            if (mBuffs.TryGetValue(e.buffName, out var icon))
            {
                GameObject.Destroy(icon.gameObject);
                mBuffs.Remove(e.buffName);
            }
        })
        .UnRegisterWhenGameObjectDestroyed(gameObject);
    }
}