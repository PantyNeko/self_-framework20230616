using UnityEngine.UI;

namespace QFramework
{
    public class BuffIcon : QF_GameController
    {
        private Text LevelText;
        private Text mTimerNum;
        private Image mMatteImg;

        private Buff mBuff;
        private BuffConfig mBuffConfig;
        private float mMaxDuration;

        private void Awake()
        {

        }
        public void Init(Buff buff, BuffConfig config)
        {
            if (mMatteImg == null)
            {
                mMatteImg = transform.Find("Matte").GetComponent<Image>();
                mTimerNum = transform.Find("Timer").GetComponent<Text>();
                LevelText = transform.Find("Level").GetComponent<Text>();
            }
            mMatteImg.fillAmount = 0;

            if (!config.IsTimeBuff)
            {
                mTimerNum.enabled = false;
            }
            mBuff = buff;
            mBuffConfig = config;
            mMaxDuration = config.Duration;

            // mMatteImg.sprite = config.Sprite;
            UpdateLevelText();
        }
        private void UpdateLevelText()
        {
            LevelText.text = mBuff.Level.ToString();
        }
        public void UpdateInfo()
        {
            // 更新计时的UI
            switch (mBuffConfig.stackType)
            {
                case E_BuffStackType.OnlyStackTime:
                case E_BuffStackType.StackTimeAndLevel:
                    mMaxDuration += mBuffConfig.Duration;
                    break;
            }
            UpdateLevelText();
        }
        private void Update()
        {
            if (mBuff.IsContinue())
            {
                mMatteImg.fillAmount = RangeTo01();
                mTimerNum.text = mBuff.duration.ToString("f2");
            }
        }
        private float RangeTo01()
        {
            // 两组映射值的差相除 乘以 (当前值 - 最小值)
            // (1-0)/ (0 - mMaxDuration) * (mBuff.duration - mMaxDuration)
            return 1 / -mMaxDuration * (mBuff.duration - mMaxDuration); // 必须最小值都为 0 的情况下
        }
    }
}