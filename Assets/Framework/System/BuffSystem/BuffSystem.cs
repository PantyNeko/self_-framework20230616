using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QFramework
{
    // 这个课程 需要大家熟悉委托 lambda表达式 里氏替换 等知识
    public class Buff
    {
        public string name { get; }
        private int level = 1;
        public int Level => level;
        public void AddHierarchy() => level++;
        public void SubHierarchy()
        {
            if (level == 1) return;
            level--;
        }
        public float duration;
        public Buff(string name)
        {
            this.name = name;
        }
        public bool IsContinue()
        {
            return duration > 0;
        }
        public void Update()
        {
            duration -= Time.deltaTime;
        }
    }
    // buff 持有者 接口
    public interface IBuffOwner : ICanSendEvent
    {
        float GetSourceValue(string buffName);
        void SetValue(string buffName, float result);
        float DataProcess(string buffName, float source);
        void SendAddBuffEvent(Buff buff, BuffConfig config);
        void SendRemoveBuffEvent(string buffName);
        void SendUpdateBuffEvent(BuffConfig config);
    }
    public enum E_BuffStackType
    {
        OnlyStackLevel,
        OnlyStackTime,
        StackTimeAndLevel,
        NotStack
    }
    // buff系统接口 
    public interface IBuffSystem : ISystem
    {
        // 添加BUFF时 需要知道 给谁添加 添加哪个Buff
        void AddBuff(string buffName, IBuffOwner owner);
        // 移除BUFF时 需要知道 给谁移除 移除哪个BUFF
        void RemoveBuff(string buffName, IBuffOwner owner);
        // 判断在某个对象中 是否包含某个buff
        bool ContainsBuff(string buffName, IBuffOwner owner);
        // 一般用于计算的Buff 我通常叫他被动BUFF
        // 提供一个用于计算某个对象身上所有同组别Buff的函数 
        double CalcPassive(IBuffOwner owner, E_BuffGroup group, float source);
        // 提供一个用于计算某个对象身上某个Buff的计算函数
        float CalcPassive(IBuffOwner owner, string buffName, float source);
        // 提供给外部获取BUFF配置信息的方法
        bool TryGetConfig(string buffName, out BuffConfig config);
    }
    public class BuffSystem : AbstractSystem, IBuffSystem
    {
        private class UpdateInfo
        {
            // 需要保存一个持有者
            private IBuffOwner Owner;
            // 对外提供一个包含方法 用于外部进行判断
            public bool Contains(IBuffOwner owner) => Owner == owner;
            // 用于存储对应的buff的计时器
            private Dictionary<Buff, DelayTask> mTasks = new Dictionary<Buff, DelayTask>();
            // 因为内部可能需要对外查询 所以这里用了一个Func委托来将逻辑进行整合设计
            // Func<string, BuffConfig> mCallBack;
            public UpdateInfo(IBuffOwner owner)
            {
                Owner = owner;
            }
            public void Add(Buff buff, BuffConfig config)
            {
                if (mTasks.ContainsKey(buff)) return;
                // 需要进行阶段性更新 可以使用计时器去计时
                var task = new DelayTask();
                task.Start(config.IntervalTime, true, () => Owner.ExcuteBuff(config, buff));
                mTasks.Add(buff, task);
            }
            public void Remove(Buff buff)
            {
                mTasks.Remove(buff);
            }
            public void Update()
            {
                foreach (var task in mTasks.Values)
                {
                    // 更新计时器
                    task.Update();
                }
            }
        }

        private Dictionary<IBuffOwner, List<Buff>> mBuffs;
        private Dictionary<string, BuffConfig> mConfigs;
        private List<UpdateInfo> mUpdateInfos;
        protected override void OnInit()
        {
            mBuffs = new Dictionary<IBuffOwner, List<Buff>>();
            mConfigs = Resources.Load<SO_BuffConfig>("SO_Data/SO_BuffConfig").ToDic();
            mUpdateInfos = new List<UpdateInfo>();

            PublicMono.Instance.OnUpdate += OnUpdate;
        }
        // 用于更新一些非计时的非被动buff
        private void OnUpdate()
        {
            for (int i = 0; i < mUpdateInfos.Count; i++)
            {
                mUpdateInfos[i].Update();
            }
        }

        public bool TryGetConfig(string buffName, out BuffConfig config)
        {
            if (mConfigs.TryGetValue(buffName, out config)) return true;
            throw new Exception($"在获取buff配置时,查询错误,Buff名字：{buffName}");
        }
        // 创建BUFF函数
        private void CreateBuff(string buffName, IBuffOwner owner, List<Buff> buffs)
        {
            // 首先获取BUFF配置 如果查询成功 说明程序跑通了
            if (TryGetConfig(buffName, out var config))
            {
                // 创建了一个buff数据
                var buff = new Buff(buffName);
                // 需要进行分支 是否是计时BUFF 需要在buff的配置中获取一个bool值
                if (config.IsTimeBuff)
                {
                    //  需要将配置里面的持续时间进行缓存 为什么呢 因为我们后续需要实现刷新buff时间的功能 
                    buff.duration = config.Duration;
                    // 计时基本都是一次的 到时间就销毁 所以这里使用协程是非常合适的
                    PublicMono.Instance.StartCoroutine(StartTime(buff, owner, config));
                }
                // 有一种非计时 但是会间断性执行的buff 这种我叫他永久性主动buff 怎么判断 不是计时又不是被动 
                else if (!config.IsPassive)
                {

                    // 查找更新列表种 是否有该持有者对象在更新
                    var upInfo = mUpdateInfos.Find(info => info.Contains(owner));
                    // 如果没有 就直接添加
                    if (upInfo == null)
                    {
                        upInfo = new UpdateInfo(owner);
                        mUpdateInfos.Add(upInfo);
                    }
                    upInfo.Add(buff, config);
                }
                // 这里需要给一个外部接口方法 用于发送当前对象对应的事件方法
                // 这里为什么不直接发送事件呢 因为事件通常是全局的 更偏向一对多
                // 如果想要进行多角色发送不同的事件 就可以让buff持有者自己实现对应的事件
                owner.SendAddBuffEvent(buff, config);
                // 将新创建的BUFF添加到buff列表中
                buffs.Add(buff);
            }
        }
        private IEnumerator StartTime(Buff buff, IBuffOwner owner, BuffConfig config)
        {
            // 如果是有时间限制的被动buff时
            if (config.IsPassive)
            {
                while (buff.IsContinue())
                {
                    buff.Update();
                    yield return null;
                }
            }
            // 如果不是
            else
            {
                // 缓存当前间隔时间 这里为什么不是0呢 因为我们第一次进入计时的时候 我就希望计时器触发一次
                float timer = config.IntervalTime;

                while (buff.IsContinue())
                {
                    buff.Update();
                    // 处理当前buff的主动效果 因为不是被动 意味着会有执行间隔
                    timer += Time.deltaTime;
                    if (timer >= config.IntervalTime)
                    {
                        // 执行buff函数
                        owner.ExcuteBuff(config, buff.Level);
                        timer = 0;
                    }
                    yield return null;
                }
            }
            // 执行结束 移除buff
            RemoveBuff(buff.name, owner);
        }
        void IBuffSystem.AddBuff(string buffName, IBuffOwner owner)
        {
            // 查询当前持有者是否在Buff系统中更新 如果没在
            if (!mBuffs.TryGetValue(owner, out var list))
            {
                // 将当前持有者添加到系统中
                list = new List<Buff>();
                mBuffs.Add(owner, list);
            }
            // 判断列表中是否有buff 如果没有就跳过查询直接创建
            if (list.Count == 0)
            {
                CreateBuff(buffName, owner, list);
                return;
            }
            // 当列表中有一些buff时 查询是否存在当前名字的buff
            // 如果存在 就尝试叠加 不存在就直接创建
            var buff = list.Find(buff => buff.name == buffName);
            // 如果buff为空 表示没有找到 直接创建
            if (buff == null)
            {
                CreateBuff(buffName, owner, list);
                return;
            }
            // 如果找到了对应的BUFF 需要先找到对应的配置表
            if (TryGetConfig(buffName, out var config))
            {
                switch (config.stackType)
                {
                    case E_BuffStackType.OnlyStackLevel:
                        // 如果叠加类型是仅叠加等级石 提升buff等级
                        buff.AddHierarchy();
                        buff.duration = config.Duration;
                        break;
                    case E_BuffStackType.OnlyStackTime:
                        buff.duration += config.Duration;
                        break;
                    case E_BuffStackType.StackTimeAndLevel:
                        buff.AddHierarchy();
                        buff.duration += config.Duration;
                        break;
                    case E_BuffStackType.NotStack:
                        // 刷新时间
                        buff.duration = config.Duration;
                        break;
                }
                owner.SendUpdateBuffEvent(config);
            }
        }
        public void RemoveBuff(string buffName, IBuffOwner owner)
        {
            // 先判断当前对象是否有某个buff
            if (mBuffs.TryGetValue(owner, out var list))
            {
                int index = list.FindIndex(buff => buff.name == buffName);
                if (index >= 0) // 说明列表中存在该对象
                {
                    owner.SendRemoveBuffEvent(buffName);
                    // var buff = list[index];
                    // 移除该位置对象
                    list.RemoveAt(index);
                }
            }
        }
        /// <summary>
        /// 计算一组被动
        /// </summary>
        /// <param name="owner">Buff持有者</param>
        /// <param name="group">Buff的分组</param>
        /// <param name="source">Buff的元数据</param>
        /// <returns>计算后的结果</returns>
        double IBuffSystem.CalcPassive(IBuffOwner owner, E_BuffGroup group, float source)
        {
            // 查询当前buff持有者身上的buff列表
            if (mBuffs.TryGetValue(owner, out var list))
            {
                // 如果没有buff返回元数据
                if (list.Count == 0) return source;
                // 一个是加减类型的buff  一个是乘除类型的buff 正常来说
                // 我们的buff最好遵循先乘除后加减的原则 这样得到的数据相对稳定
                var stack = new Stack<(float, E_Operator)>();
                // 对buff列表进行遍历
                for (int i = 0; i < list.Count; i++)
                {
                    // 找到当前buff的配置信息
                    if (TryGetConfig(list[i].name, out var config))
                    {
                        // 找到对应的被动buff 并且 分组相同的话 就可以让该buff进行被动计算
                        if (config.IsPassive && config.Group == group)
                        {
                            // 获取当前buff需要参与计算的增量 元数据 需要乘上当前buff的等级
                            float delta = owner.DataProcess(config.Name, config.Value * list[i].Level);
                            // 根据不同的运算符 进行不同的计算操作
                            switch (config.Operator)
                            {
                                case E_Operator.Add:
                                case E_Operator.Sub:
                                    stack.Push((delta, config.Operator));
                                    break;
                                // 乘法和除法大部分都是百分比 所以 我们需要加入一个特殊的计算方式
                                // 例如增加百分之 20 就是 乘以 1 + 0.2f
                                // 例如减少百分之 20 就是 乘以 1 - 0.2f
                                case E_Operator.Mul:
                                    source *= 1 + delta;
                                    break;
                                case E_Operator.Div:
                                    source *= 1 - delta;
                                    break;
                            }
                        }
                    }
                }
                // 看看有没有加法的buff
                while (stack.Count > 0)
                {
                    var info = stack.Pop();
                    switch (info.Item2)
                    {
                        case E_Operator.Add:
                            source += info.Item1;
                            break;
                        case E_Operator.Sub:
                            source -= info.Item1;
                            break;
                    }
                }
            }
            return source;
        }
        // 计算单个被动
        float IBuffSystem.CalcPassive(IBuffOwner owner, string buffName, float source)
        {
            // 查询buff 列表
            if (mBuffs.TryGetValue(owner, out var list))
            {
                if (list.Count == 0) return source;
                // 查询列表中是否存在buff
                int index = list.FindIndex(info => info.name == buffName);
                // 需要找到对应buff 并且 能够找到配置 并且 buff是被动buff
                if (index >= 0 && TryGetConfig(buffName, out var config) && config.IsPassive)
                {
                    owner.CalcPassive(config, list[index].Level, ref source);
                }
            }
            return source;
        }

        public bool ContainsBuff(string buffName, IBuffOwner owner)
        {
            return mBuffs.TryGetValue(owner, out var list) ? list.Find(buff => buff.name == buffName) != null : false;
        }
    }
}