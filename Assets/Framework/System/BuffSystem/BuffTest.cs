using QFramework;
using UnityEngine;

public struct AddTestBuffEvent
{
    public Buff buff;
    public BuffConfig config;
}
public struct RemoveTestBuffEvent
{
    public string buffName;
}
public struct UpdateTestBuffEvent
{
    public BuffConfig config;
}
public class BuffID
{
    public const string 中毒 = "中毒";
    public const string 持续回血 = "持续回血";
    public const string 百分比暴击 = "暴击%";
    public const string 百分比削弱 = "削弱%";
}
public class BuffTest : QF_GameController, IBuffOwner
{
    [SerializeField] private float HP = 200;
    private float Attack = 10;
    [SerializeField] private float mCurAttack = 0;

    private void Start()
    {
        mCurAttack = Attack;
    }
    void IBuffOwner.SendRemoveBuffEvent(string buffName)
    {
        this.SendEvent(new RemoveTestBuffEvent() { buffName = buffName });
    }
    float IBuffOwner.GetSourceValue(string buffName)
    {
        switch (buffName)
        {
            case BuffID.中毒: return HP;
            case BuffID.持续回血: return HP;
            case BuffID.百分比暴击: return Attack;
            case BuffID.百分比削弱: return Attack;
        }
        return 0f;
    }
    void IBuffOwner.SetValue(string buffName, float result)
    {
        switch (buffName)
        {
            case BuffID.中毒: HP = result; break;
            case BuffID.持续回血: HP = result; break;
            case BuffID.百分比暴击: mCurAttack = result; break;
            case BuffID.百分比削弱: mCurAttack = result; break;
        }
    }
    void IBuffOwner.SendUpdateBuffEvent(BuffConfig config)
    {
        this.SendEvent(new UpdateTestBuffEvent() { config = config });
    }
    void IBuffOwner.SendAddBuffEvent(Buff buff, BuffConfig config)
    {
        this.SendEvent(new AddTestBuffEvent() { buff = buff, config = config });
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            this.GetSystem<IBuffSystem>().AddBuff(BuffID.中毒, this);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            this.GetSystem<IBuffSystem>().AddBuff(BuffID.持续回血, this);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            this.GetSystem<IBuffSystem>().AddBuff(BuffID.百分比暴击, this);
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            this.GetSystem<IBuffSystem>().AddBuff(BuffID.百分比削弱, this);
        }
    }
    // 这个数据处理有什么用呢？？ 假设 我们在buff的基础上 需要通过一些特殊的公式去变更buff的实际能力
    // 例如说 我装备了一件可以增加两倍爆击加成的装备 我们可以在暴击的基础数据上 让加值 x 2
    public float DataProcess(string buffName, float source)
    {
        return buffName switch
        {
            BuffID.中毒 => source,
            BuffID.持续回血 => source,
            BuffID.百分比暴击 => source,
            BuffID.百分比削弱 => source,
            _ => source
        };
    }
}