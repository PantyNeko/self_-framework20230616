using UnityEngine;

namespace QFramework.EquipmentSystem
{
    [CreateAssetMenu(fileName = "New GunConfig", menuName = "Data/SO/GunConfig")]
    public class SO_GunConfig : SO_EquipConfig<GunConfig> { }
}