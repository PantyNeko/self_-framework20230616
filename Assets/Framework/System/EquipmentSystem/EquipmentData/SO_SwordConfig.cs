using UnityEngine;

namespace QFramework.EquipmentSystem
{
    [CreateAssetMenu(fileName = "New SwordConfig", menuName = "Data/SO/SwordConfig")]
    public class SO_SwordConfig : SO_EquipConfig<SwordConfig> { }
}