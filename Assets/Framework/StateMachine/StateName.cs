﻿namespace Panty
{
    public class StateName
    {
        public const string Idle = "Idle";
        public const string Move = "Move";
        public const string Jump = "Jump";
        public const string InAir = "InAir";
        public const string Land = "Land";
        public const string Hit = "Hit";
        public const string Crouch = "Crouch";
        public const string LookUp = "LookUp";
        public const string WallGrab = "WallGrab";
        public const string WallClimb = "WallClimb";
        public const string WallSlide = "WallSlide";
        public const string LedgeClimb = "LedgeClimb";
    }
}