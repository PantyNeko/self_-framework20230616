using QFramework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class 子弹 : QF_GameController
{
    private int dir; // +-1
    public void SetDir(int dir)
    {
        this.dir = dir;
    }
    private void Update()
    {
        transform.Translate(dir * 5f * Time.deltaTime, 0, 0);
        var col = Physics2D.OverlapBox(transform.position, transform.localScale, 0, ~LayerMask.GetMask("子弹"));
        if (col)
        {
            var targte = col.GetComponent<Player>();
            if (targte)
            {
                Player.Hp.Value -= 5;
            }
            else
            {
                Enemy.Hp.Value -= 5;
                
            }
            this.GetSystem<IMessageSystem>().Log($"生命-5,剩余{(targte ? Player.Hp.Value : Enemy.Hp.Value)}血,");
            // this.GetSystem<IMessageSystem>().Send($"生命-5,剩余{(targte ? Player.Hp.Value : Enemy.Hp.Value)}血,", transform.position);
            GameObject.Destroy(gameObject);
        }
    }
}