using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using QFramework;

public class 装备系统测试 : AbsEquipSlot,IController
{
    // 用来准备装备数据 并 使用
    protected override void Use(EquipInfo info, byte abilityIndex)
    {
        switch (info.Type)
        {
            case E_EquipType.Gun:
                // 这个可以写在单独的枪械系统中
                switch (abilityIndex)
                {
                    case 0:
                        Debug.Log("射击");
                        break;
                    case 1:
                        Debug.Log("装填");
                        break;
                    case 2:
                        Debug.Log("强力子弹");
                        break;
                }
                break;
            case E_EquipType.Sword:
                switch (abilityIndex)
                {
                    case 0:
                        Debug.Log("挥砍");
                        break;
                    case 1:
                        Debug.Log("爆发");
                        break;
                }
                break;
        }
    }

    IArchitecture IBelongToArchitecture.GetArchitecture()
    {
        return QF_Game.Interface;
    }

    private void Start()
    {
        var list = this.GetSystem<IEquipmentFactorySystem>().CreateAllEquip();
        mEquips = new LoopList<EquipInfo>(list);
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            Use(0);
        }
        if (Input.GetKeyDown(KeyCode.X))
        {
            Use(1);
        }
        if (Input.GetKeyDown(KeyCode.C))
        {
            Use(2);
        }
        if (Input.GetKeyDown(KeyCode.Q))
        {
            Change(false);
        }
    }
}