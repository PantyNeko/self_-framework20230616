using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace QFramework
{
    public class BagPanel : QF_UIController, IPointerDownHandler, IDragHandler, IPointerUpHandler
    {
        [SerializeField] private Sprite mEmptyCell;
        [SerializeField] private Sprite mDisableCell;

        private List<ItemCell> mCells;
        private Dictionary<string, E_BagItemType> mPages;

        private int vernier;

        private ItemCell mSelectCell;
        private ItemSelector mItemSelector;
        private Image mSelectItemIcon;
        private Text mCoinNumTex;

        private int rowsNum;
        private int pageSize;

        private IInventorySystem mInventory;

        protected override void Awake()
        {
            base.Awake();

            mInventory = this.GetSystem<IInventorySystem>();

            mItemSelector = GetComponentInChildren<ItemSelector>();
            mSelectItemIcon = transform.Find("SelectItemIcon").GetComponent<Image>();
            mSelectItemIcon.enabled = false;

            this.FindChildrenControl<Text>((name, control) =>
            {
                if (name == "CoinNum")
                {
                    mCoinNumTex = control;
                }
            });

            this.RegisterEvent<SwitchBagPagesEvent>(e => MoveCellSelector(e.index % pageSize)).UnRegisterWhenGameObjectDestroyed(gameObject);
            this.GetModel<IPlayerResModel>().CoinNum.RegisterWithInitValue(num => mCoinNumTex.text = num.ToString()).UnRegisterWhenGameObjectDestroyed(gameObject);
            this.RegisterEvent<ItemChangeEvent>(e =>
            {
                var cell = mCells[e.index % pageSize];
                cell.EnableCell(e.info.id >= 0);
                switch (e.info.id)
                {
                    case -1:
                        cell.SetCellBg(mEmptyCell);
                        break;
                    case -2:
                        cell.SetCellBg(mDisableCell);
                        break;
                    default:
                        cell.SetItem(mInventory.GetItemConfig(e.info).sprite, e.info.count);
                        cell.SetCellBg(mEmptyCell);
                        break;
                }
            })
            .UnRegisterWhenGameObjectDestroyed(gameObject);
        }
        protected override void OnEnable()
        {
            base.OnEnable();
            this.RegisterEvent<DirInputEvent>(OnDirInput);
            this.RegisterEvent<InteractiveKeyInputEvent>(OnInteractiveKeyInput);
        }
        protected override void OnDisable()
        {
            base.OnDisable();
            this.UnRegisterEvent<DirInputEvent>(OnDirInput);
            this.UnRegisterEvent<InteractiveKeyInputEvent>(OnInteractiveKeyInput);
        }
        private void OnInteractiveKeyInput(InteractiveKeyInputEvent e)
        {
            if (mSelectCell == null)
            {
                if (mInventory.GetItemByCell(mCells[vernier].ID, out var info) && info.id >= 0)
                {
                    mSelectCell = mCells[vernier];
                    mSelectCell.ChangeSelectState(true);
                }
            }
            else
            {
                // 当前ID必须与游标ID不同 才做处理
                if (mSelectCell.ID.Equals(vernier))
                {
                    mInventory.RemoveItemByCell(mSelectCell.ID);
                }
                else
                {
                    mInventory.MoveItemByCell(mSelectCell.ID, vernier);
                }
                mSelectCell.ChangeSelectState(false);
                mSelectCell = null;
            }
        }
        private void OnDirInput(DirInputEvent e)
        {
            var dir = e.dir;
            // 移动选择框 无输入时 不进行处理
            // if (dir.Equals(Vector2.zero)) return;
            var absX = Mathf.Abs(dir.x);
            var absY = Mathf.Abs(dir.y);
            // 当方向的XY都有值时说明时斜方向 不允许
            // if (absX > 0 && absY > 0) return;
            // 当X方向有值 Y 方向没有值时
            if (absX > 0 && absY < 0.1f)
            {
                if (dir.x > 0)
                {
                    // 小于当前页的可使用数量
                    if (mInventory.IsInPageRange(vernier))
                    {
                        MoveCellSelector(vernier + 1);
                    }
                }
                else
                {
                    var next = vernier - 1;
                    if (next >= 0) MoveCellSelector(next);
                }
            }
            // 这里的 0.1 主要在优化手柄
            else if (absX < 0.1f && absY > 0)
            {
                if (dir.y > 0)
                {
                    var next = vernier - rowsNum;
                    if (next >= 0) MoveCellSelector(next);
                }
                else
                {
                    var next = vernier + rowsNum;
                    if (mInventory.IsInPageRange(next - 1))
                    {
                        MoveCellSelector(next);
                    }
                }
            }
        }
        protected override void OnValueChanged(string togName, bool isOn)
        {
            if (isOn && mPages.TryGetValue(togName, out var type))
            {
                mInventory.SwitchType(type);
            }
        }
        protected override void OnClick(string btnName)
        {
            this.GetSystem<IUGUISystem>().HidePanel<BagPanel>(true);
        }
        public override void ShowMe()
        {
            base.ShowMe();
            // 将选择器移动到初始位置
            vernier = 0;
            // 重置选择器位置
            mItemSelector.ResetPos();
            // 如果格子数组为空 说明第一次加载背包 需要进行初始化
            if (mCells == null && mInventory.GetOrCreateItemConfig(out var config))
            {
                pageSize = config.PageSize;
                rowsNum = config.RowCount;
                // 计算每页的固定数量 初始化格子列表
                mCells = new List<ItemCell>(pageSize);
                // 初始化主容器
                var itemBg = transform.Find("ItemBg");
                var itemList = itemBg.Find("ItemList") as RectTransform;
                var itemView = itemList.Find("ItemView") as RectTransform;
                itemView.sizeDelta = itemList.sizeDelta = config.ViewSize;

                mPages = new Dictionary<string, E_BagItemType>();
                PEnum.Loop<E_BagItemType>(type =>
                {
                    if ((type & config.types) == 0) return;
                    mPages.Add(type.ToString(), type);
                });
                var bagTogGroup = itemBg.Find("BagPageTog") as RectTransform;
                if (mPages.Count <= 1)
                {
                    itemList.anchoredPosition += Vector2.up * bagTogGroup.sizeDelta.y;
                    GameObject.Destroy(bagTogGroup.gameObject);
                    mPages = null;
                }
                else
                {
                    bagTogGroup.sizeDelta = new Vector2(itemList.sizeDelta.x, bagTogGroup.sizeDelta.y);
                    bagTogGroup.anchoredPosition = new Vector2(0, bagTogGroup.anchoredPosition.y);
                    var tempTog = bagTogGroup.Find("Toggle") as RectTransform;
                    float step = bagTogGroup.sizeDelta.x / mPages.Count;
                    tempTog.sizeDelta = new Vector2(step, tempTog.sizeDelta.y);
                    int num = 0;
                    var togs = new List<Toggle>(mPages.Count - 1);
                    Toggle _tog = null;
                    foreach (var type in mPages.Values)
                    {
                        var typeStr = type.ToString();
                        var o = num == 0 ? tempTog.gameObject : GameObject.Instantiate(tempTog.gameObject, bagTogGroup);
                        var togTrans = o.transform as RectTransform;
                        togTrans.anchoredPosition = new Vector2(step * num++, tempTog.anchoredPosition.y);
                        o.transform.GetChild(0).name = typeStr + "Bg";
                        o.name = typeStr;
                        var textCtrl = o.GetComponentInChildren<Text>();
                        textCtrl.name = typeStr + "Text";
                        textCtrl.text = typeStr;
                        var tog = o.GetComponent<Toggle>();
                        tog.onValueChanged.AddListener(isOn => OnValueChanged(typeStr, isOn));
                        if (type == config.defaultType)
                        {
                            _tog = tog;
                        }
                        else togs.Add(tog);
                    }
                    if (_tog == null)
                        throw new System.Exception($"{config.defaultType}没有在配置枚举列表中");
                    var group = bagTogGroup.GetComponent<ToggleGroup>();
                    _tog.group = group;
                    _tog.isOn = true;
                    for (int i = 0; i < togs.Count; i++)
                    {
                        togs[i].group = group;
                    }
                }
                // 初始化选择器大小
                var cellSize = config.CellSize;
                mItemSelector.InitSize(cellSize);
                // 根据数据 创建对应数量格子
                for (int i = 0; i < pageSize; i++)
                {
                    var o = ResHelper.SyncLoad<GameObject>("Prefabs/ItemCell");
                    o.transform.SetParent(itemView);
                    var cellTrans = o.transform as RectTransform;
                    cellTrans.sizeDelta = cellSize;
                    cellTrans.anchoredPosition = config.GetPos(i);
                    cellTrans.localScale = Vector2.one;
                    // 更新格子数据
                    var cell = o.GetComponent<ItemCell>();
                    cell.ID = mCells.Count;
                    mCells.Add(cell);
                }
            }
        }
        private void MoveCellSelector(int targetIndex)
        {
            // 如果ID相同 不进行移动
            if (vernier == targetIndex) return;
            // 确保索引合法
            if (mItemSelector.CanStartMove)
            {
                vernier = targetIndex;
                mItemSelector.SetTarget(mCells[vernier].transform);
            }
        }
        void IPointerDownHandler.OnPointerDown(PointerEventData e)
        {
            // 必须是左键按下
            if (e.button == PointerEventData.InputButton.Left)
            {
                // 必须是有对象才能操作
                var o = e.pointerPressRaycast.gameObject;
                mSelectCell = o?.GetComponent<ItemCell>();
                if (mSelectCell == null) return;
                // 查询ID内容
                if (mInventory.GetItemByCell(mSelectCell.ID, out var info) && info.id >= 0)
                {
                    mSelectItemIcon.enabled = true;
                    mSelectCell.EnableCell(false);
                    mSelectItemIcon.sprite = mInventory.GetItemConfig(info).sprite;
                    mSelectItemIcon.transform.position = e.position;
                }
                else
                {
                    mSelectCell = null;
                }
            }
        }
        void IDragHandler.OnDrag(PointerEventData e)
        {
            if (mSelectCell == null) return;
            mSelectItemIcon.transform.position = e.position;
        }
        void IPointerUpHandler.OnPointerUp(PointerEventData e)
        {
            if (mSelectCell == null) return;
            if (EventSystem.current.IsPointerOverGameObject())
            {
                var cell = e.pointerCurrentRaycast.gameObject.GetComponent<ItemCell>();
                if (cell != null)
                {
                    if (cell == mSelectCell)
                    {
                        mInventory.ResetItemByCell(mSelectCell.ID);
                    }
                    else
                    {
                        mInventory.MoveItemByCell(mSelectCell.ID, cell.ID);
                    }
                }
            }
            else
            {
                mInventory.RemoveItemByCell(mSelectCell.ID);
            }
            mSelectItemIcon.enabled = false;
            mSelectCell = null;
        }
    }
}