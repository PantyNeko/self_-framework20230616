﻿using QFramework;
using UnityEngine;

public interface IGunSystem : ISystem
{
    void Shoot(Vector2 dir);
}
public class GunSystem : AbstractSystem, IGunSystem
{
    protected override void OnInit() { }

    void IGunSystem.Shoot(Vector2 dir)
    {
        Debug.Log($"当前方向为{dir},完毕 射击");
    }
}