using QFramework;
using System.Collections.Generic;
using UnityEngine;

public interface ITestSystem : ISystem
{
    int GetNum(int index);
}
[CreateAssetMenu(fileName = "New TestSystem", menuName = "Data/SO/TestSystem")]
public class TestSystem : AbstractSystemBySO, ITestSystem, ISaveMark<Save1>
{
    [SerializeField] private List<int> nums;
    protected override void OnInit()
    {
        this.GetSystem<IArchiveSystem>().Add(this);
        // nums = new List<int>() { 1, 2, 3 };
    }
    int ITestSystem.GetNum(int index)
    {
        return index >= 0 && index < nums.Count ? nums[index] : -1;
    }

    void ISaveMark<Save1>.Load(Save1 archive)
    {
        this.GetModel<IPlayerModel>().HP.Value = archive.hp;
        nums = archive.nums;
    }
    void ISaveMark<Save1>.Save(Save1 archive)
    {
        archive.nums = nums;
        archive.hp = this.GetModel<IPlayerModel>().HP.Value;
    }
}