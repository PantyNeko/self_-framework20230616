﻿using QFramework;
using UnityEngine;

public class ShootCommand : AbstractCommand<int>
{
    private readonly Vector2 dir;

    public ShootCommand(Vector2 dir)
    {
        this.dir = dir;
    }
    public ShootCommand() { }
    protected override int OnExecute()
    {
        var hp = this.GetModel<IPlayerModel>().HP;
        hp.Value--;
        if (hp.Value == 0)
        {
            this.SendEvent<PlayerDieEvent>();
        }
        else
        {
            this.GetSystem<IGunSystem>().Shoot(dir);
        }
        return -1;
    }
}