using System;
using UnityEngine;
using QFramework;

public interface IBagConfigModel : IModel
{
    bool GetById(int id, out BagConfig config);
}
[Serializable]
public class BagConfig
{
    public string name;
    public Sprite sprite;
    public int stack;
}
[CreateAssetMenu(fileName = "New BagConfigModel", menuName = "Data/SO/BagConfigModel")]
public class BagConfigModel : AbstractConfigModelBySO, IBagConfigModel
{
    [SerializeField] private BagConfig[] mConfig;

    bool IBagConfigModel.GetById(int id, out BagConfig config)
    {
        config = id >= 0 && id < mConfig.Length ? mConfig[id] : null;
        return config != null;
    }
}