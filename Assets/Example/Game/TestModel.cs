using QFramework;
using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class BagInfo
{
    public int id;
    public int count;
}
public interface ITestModel : IModel
{
    void AddInfo(int id, int count);
}
[CreateAssetMenu(fileName = "New TestModel", menuName = "Data/SO/TestModel")]
public class TestModel : AbstractModelBySO, ITestModel
{
    [SerializeField] private List<BagInfo> BagInfos;
    protected override void OnInit()
    {
        BagInfos = new List<BagInfo>()
        {
            new BagInfo(){id = 0,count = 1}
        };
    }
    void ITestModel.AddInfo(int id, int count)
    {
        BagInfos.Add(new BagInfo()
        {
            id = id,
            count = count
        });
    }
}