//------------------------------------------------------------------------------
// <auto-generated>
//     This code was auto-generated by com.unity.inputsystem:InputActionCodeGenerator
//     version 1.4.1
//     from Assets/Project/SOData/PlayerInputActions.inputactions
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public partial class @PlayerInputActions : IInputActionCollection2, IDisposable
{
    public InputActionAsset asset { get; }
    public @PlayerInputActions()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""PlayerInputActions"",
    ""maps"": [
        {
            ""name"": ""GamePlay"",
            ""id"": ""cdd99668-e4ef-481e-849b-55b30bb7f53a"",
            ""actions"": [
                {
                    ""name"": ""TapMiddle"",
                    ""type"": ""PassThrough"",
                    ""id"": ""22794864-fb4f-4715-b624-123128198283"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""TapRight"",
                    ""type"": ""PassThrough"",
                    ""id"": ""86babb9a-9456-4b41-877f-159a4579ce67"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""TapLeft"",
                    ""type"": ""PassThrough"",
                    ""id"": ""86ee6acf-d737-469c-945f-1be6a5810b4d"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""MousePos"",
                    ""type"": ""PassThrough"",
                    ""id"": ""e0dfeb94-dfc6-4b3b-bb75-3acc95ae08a6"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""MouseDelta"",
                    ""type"": ""PassThrough"",
                    ""id"": ""57fa2488-ff79-4539-b8ac-a1c0bd882444"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""MouseScroll"",
                    ""type"": ""PassThrough"",
                    ""id"": ""2f6e86a3-92f8-46d7-bfa3-ff0a8207abe1"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""Move"",
                    ""type"": ""PassThrough"",
                    ""id"": ""714bbb49-186b-4a7f-8bf6-83f4d4d0e8b9"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""Interactive"",
                    ""type"": ""Button"",
                    ""id"": ""bf1475af-01a2-4a60-9eff-f63cb2961c9f"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""Attack"",
                    ""type"": ""PassThrough"",
                    ""id"": ""2b1d6e75-5dd3-4258-84e6-7d6214488e33"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""SwitchUp"",
                    ""type"": ""Button"",
                    ""id"": ""92a7a37d-5eb9-4569-8932-1d2dd9007885"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""32ab30c5-30e1-495f-b357-401c49f1483d"",
                    ""path"": ""<Mouse>/middleButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""TapMiddle"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""cb68addd-b013-4140-ad02-98a1b2df7086"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""TapRight"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3e8b5b51-57a5-4293-abc8-cd16ac7c5b9d"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""TapLeft"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""609a1ae2-637c-4b4e-9a0c-eb10794c0be0"",
                    ""path"": ""<Mouse>/position"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MousePos"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a21ae657-ad47-4549-bfa3-6b65f5037ce1"",
                    ""path"": ""<Mouse>/delta"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MouseDelta"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ae929c31-4e82-49bb-b47d-fa3e9cb10218"",
                    ""path"": ""<Mouse>/scroll/y"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MouseScroll"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""WASD"",
                    ""id"": ""febc9eee-e6ae-44ad-a434-efb8111cc323"",
                    ""path"": ""2DVector(mode=1)"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""4439dcf7-76e5-4317-bb18-332c3ba392f2"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""9dca2feb-4a58-4864-b674-49dea34de9f6"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""910f636c-485a-4bbc-bf7b-5c80b2e27c5a"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""3d8601ae-9c1a-4c28-b71d-03cab1197743"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""a2fe3571-9608-40b7-9e47-727f45a42c28"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Interactive"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""52cb9608-9d36-4a74-87bf-e517636059a1"",
                    ""path"": ""<Keyboard>/j"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Attack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""21427e4c-bbea-4245-9965-2472acd0a182"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Attack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e9050dbc-ad01-493c-a92c-2eecb68163d9"",
                    ""path"": ""<Keyboard>/q"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SwitchUp"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // GamePlay
        m_GamePlay = asset.FindActionMap("GamePlay", throwIfNotFound: true);
        m_GamePlay_TapMiddle = m_GamePlay.FindAction("TapMiddle", throwIfNotFound: true);
        m_GamePlay_TapRight = m_GamePlay.FindAction("TapRight", throwIfNotFound: true);
        m_GamePlay_TapLeft = m_GamePlay.FindAction("TapLeft", throwIfNotFound: true);
        m_GamePlay_MousePos = m_GamePlay.FindAction("MousePos", throwIfNotFound: true);
        m_GamePlay_MouseDelta = m_GamePlay.FindAction("MouseDelta", throwIfNotFound: true);
        m_GamePlay_MouseScroll = m_GamePlay.FindAction("MouseScroll", throwIfNotFound: true);
        m_GamePlay_Move = m_GamePlay.FindAction("Move", throwIfNotFound: true);
        m_GamePlay_Interactive = m_GamePlay.FindAction("Interactive", throwIfNotFound: true);
        m_GamePlay_Attack = m_GamePlay.FindAction("Attack", throwIfNotFound: true);
        m_GamePlay_SwitchUp = m_GamePlay.FindAction("SwitchUp", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }
    public IEnumerable<InputBinding> bindings => asset.bindings;

    public InputAction FindAction(string actionNameOrId, bool throwIfNotFound = false)
    {
        return asset.FindAction(actionNameOrId, throwIfNotFound);
    }
    public int FindBinding(InputBinding bindingMask, out InputAction action)
    {
        return asset.FindBinding(bindingMask, out action);
    }

    // GamePlay
    private readonly InputActionMap m_GamePlay;
    private IGamePlayActions m_GamePlayActionsCallbackInterface;
    private readonly InputAction m_GamePlay_TapMiddle;
    private readonly InputAction m_GamePlay_TapRight;
    private readonly InputAction m_GamePlay_TapLeft;
    private readonly InputAction m_GamePlay_MousePos;
    private readonly InputAction m_GamePlay_MouseDelta;
    private readonly InputAction m_GamePlay_MouseScroll;
    private readonly InputAction m_GamePlay_Move;
    private readonly InputAction m_GamePlay_Interactive;
    private readonly InputAction m_GamePlay_Attack;
    private readonly InputAction m_GamePlay_SwitchUp;
    public struct GamePlayActions
    {
        private @PlayerInputActions m_Wrapper;
        public GamePlayActions(@PlayerInputActions wrapper) { m_Wrapper = wrapper; }
        public InputAction @TapMiddle => m_Wrapper.m_GamePlay_TapMiddle;
        public InputAction @TapRight => m_Wrapper.m_GamePlay_TapRight;
        public InputAction @TapLeft => m_Wrapper.m_GamePlay_TapLeft;
        public InputAction @MousePos => m_Wrapper.m_GamePlay_MousePos;
        public InputAction @MouseDelta => m_Wrapper.m_GamePlay_MouseDelta;
        public InputAction @MouseScroll => m_Wrapper.m_GamePlay_MouseScroll;
        public InputAction @Move => m_Wrapper.m_GamePlay_Move;
        public InputAction @Interactive => m_Wrapper.m_GamePlay_Interactive;
        public InputAction @Attack => m_Wrapper.m_GamePlay_Attack;
        public InputAction @SwitchUp => m_Wrapper.m_GamePlay_SwitchUp;
        public InputActionMap Get() { return m_Wrapper.m_GamePlay; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(GamePlayActions set) { return set.Get(); }
        public void SetCallbacks(IGamePlayActions instance)
        {
            if (m_Wrapper.m_GamePlayActionsCallbackInterface != null)
            {
                @TapMiddle.started -= m_Wrapper.m_GamePlayActionsCallbackInterface.OnTapMiddle;
                @TapMiddle.performed -= m_Wrapper.m_GamePlayActionsCallbackInterface.OnTapMiddle;
                @TapMiddle.canceled -= m_Wrapper.m_GamePlayActionsCallbackInterface.OnTapMiddle;
                @TapRight.started -= m_Wrapper.m_GamePlayActionsCallbackInterface.OnTapRight;
                @TapRight.performed -= m_Wrapper.m_GamePlayActionsCallbackInterface.OnTapRight;
                @TapRight.canceled -= m_Wrapper.m_GamePlayActionsCallbackInterface.OnTapRight;
                @TapLeft.started -= m_Wrapper.m_GamePlayActionsCallbackInterface.OnTapLeft;
                @TapLeft.performed -= m_Wrapper.m_GamePlayActionsCallbackInterface.OnTapLeft;
                @TapLeft.canceled -= m_Wrapper.m_GamePlayActionsCallbackInterface.OnTapLeft;
                @MousePos.started -= m_Wrapper.m_GamePlayActionsCallbackInterface.OnMousePos;
                @MousePos.performed -= m_Wrapper.m_GamePlayActionsCallbackInterface.OnMousePos;
                @MousePos.canceled -= m_Wrapper.m_GamePlayActionsCallbackInterface.OnMousePos;
                @MouseDelta.started -= m_Wrapper.m_GamePlayActionsCallbackInterface.OnMouseDelta;
                @MouseDelta.performed -= m_Wrapper.m_GamePlayActionsCallbackInterface.OnMouseDelta;
                @MouseDelta.canceled -= m_Wrapper.m_GamePlayActionsCallbackInterface.OnMouseDelta;
                @MouseScroll.started -= m_Wrapper.m_GamePlayActionsCallbackInterface.OnMouseScroll;
                @MouseScroll.performed -= m_Wrapper.m_GamePlayActionsCallbackInterface.OnMouseScroll;
                @MouseScroll.canceled -= m_Wrapper.m_GamePlayActionsCallbackInterface.OnMouseScroll;
                @Move.started -= m_Wrapper.m_GamePlayActionsCallbackInterface.OnMove;
                @Move.performed -= m_Wrapper.m_GamePlayActionsCallbackInterface.OnMove;
                @Move.canceled -= m_Wrapper.m_GamePlayActionsCallbackInterface.OnMove;
                @Interactive.started -= m_Wrapper.m_GamePlayActionsCallbackInterface.OnInteractive;
                @Interactive.performed -= m_Wrapper.m_GamePlayActionsCallbackInterface.OnInteractive;
                @Interactive.canceled -= m_Wrapper.m_GamePlayActionsCallbackInterface.OnInteractive;
                @Attack.started -= m_Wrapper.m_GamePlayActionsCallbackInterface.OnAttack;
                @Attack.performed -= m_Wrapper.m_GamePlayActionsCallbackInterface.OnAttack;
                @Attack.canceled -= m_Wrapper.m_GamePlayActionsCallbackInterface.OnAttack;
                @SwitchUp.started -= m_Wrapper.m_GamePlayActionsCallbackInterface.OnSwitchUp;
                @SwitchUp.performed -= m_Wrapper.m_GamePlayActionsCallbackInterface.OnSwitchUp;
                @SwitchUp.canceled -= m_Wrapper.m_GamePlayActionsCallbackInterface.OnSwitchUp;
            }
            m_Wrapper.m_GamePlayActionsCallbackInterface = instance;
            if (instance != null)
            {
                @TapMiddle.started += instance.OnTapMiddle;
                @TapMiddle.performed += instance.OnTapMiddle;
                @TapMiddle.canceled += instance.OnTapMiddle;
                @TapRight.started += instance.OnTapRight;
                @TapRight.performed += instance.OnTapRight;
                @TapRight.canceled += instance.OnTapRight;
                @TapLeft.started += instance.OnTapLeft;
                @TapLeft.performed += instance.OnTapLeft;
                @TapLeft.canceled += instance.OnTapLeft;
                @MousePos.started += instance.OnMousePos;
                @MousePos.performed += instance.OnMousePos;
                @MousePos.canceled += instance.OnMousePos;
                @MouseDelta.started += instance.OnMouseDelta;
                @MouseDelta.performed += instance.OnMouseDelta;
                @MouseDelta.canceled += instance.OnMouseDelta;
                @MouseScroll.started += instance.OnMouseScroll;
                @MouseScroll.performed += instance.OnMouseScroll;
                @MouseScroll.canceled += instance.OnMouseScroll;
                @Move.started += instance.OnMove;
                @Move.performed += instance.OnMove;
                @Move.canceled += instance.OnMove;
                @Interactive.started += instance.OnInteractive;
                @Interactive.performed += instance.OnInteractive;
                @Interactive.canceled += instance.OnInteractive;
                @Attack.started += instance.OnAttack;
                @Attack.performed += instance.OnAttack;
                @Attack.canceled += instance.OnAttack;
                @SwitchUp.started += instance.OnSwitchUp;
                @SwitchUp.performed += instance.OnSwitchUp;
                @SwitchUp.canceled += instance.OnSwitchUp;
            }
        }
    }
    public GamePlayActions @GamePlay => new GamePlayActions(this);
    public interface IGamePlayActions
    {
        void OnTapMiddle(InputAction.CallbackContext context);
        void OnTapRight(InputAction.CallbackContext context);
        void OnTapLeft(InputAction.CallbackContext context);
        void OnMousePos(InputAction.CallbackContext context);
        void OnMouseDelta(InputAction.CallbackContext context);
        void OnMouseScroll(InputAction.CallbackContext context);
        void OnMove(InputAction.CallbackContext context);
        void OnInteractive(InputAction.CallbackContext context);
        void OnAttack(InputAction.CallbackContext context);
        void OnSwitchUp(InputAction.CallbackContext context);
    }
}
