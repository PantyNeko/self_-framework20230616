using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;
using System.IO;

namespace Panty
{
    public abstract class PnEditor<T> : EditorWindow where T : Enum
    {
        protected enum E_Path : byte
        {
            PersistentDataPath,
            StreamingAssetsPath,
            TemporaryCachePath,
            DesktopPath,
            DocumentsPath,
            DataPath,
            SelectPath,
            Custom,
        }
        protected string inputText = "Welcome!";

        private T modes;
        private E_Path mPath;
        protected bool mIsLockBtn = true, mShowBaseInfo, mDisabledInputBox = true, mCanInit = true;

        private KeyValuePair<string, Action>[] btnInfos;
        private KeyValuePair<string, Action>[] mLockableBtn;

        private GUIContent[] mMenuItemContent;
        private GenericMenu.MenuFunction[] mMenuItemFunc;

        private const float textSpacing = 8f;
        private const byte MaxLineItemCount = 4;

        protected abstract T Empty { get; }
        protected abstract void ExecuteMode(T mode);
        private void OnEnable()
        {
            mCanInit = true;
        }
        private void Update()
        {
            if (EqualityComparer<T>.Default.Equals(modes, Empty)) return;
            ExecuteMode(modes);
        }
        private void OnInspectorUpdate()
        {
            if (EditorApplication.isPlaying) Repaint();
        }
        private void InitializeGUI()
        {
            btnInfos = InitBtnInfo(out mLockableBtn);
            var menu = RightClickMenu();
            if (menu != null && menu.Length > 0)
            {
                mMenuItemContent = new GUIContent[menu.Length];
                mMenuItemFunc = new GenericMenu.MenuFunction[menu.Length];
                for (byte i = 0; i < menu.Length; i++)
                {
                    mMenuItemContent[i] = new GUIContent(menu[i].Key);
                    mMenuItemFunc[i] = menu[i].Value;
                }
            }
            float len = btnInfos == null ? 0f : btnInfos.Length;
            // 计算最小窗口宽度为按钮宽度的总和加上间隔，再加上额外宽度
            float buttonWidth = GUI.skin.button.CalcSize(new GUIContent("0000000000")).x;
            float minWindowWidth = MaxLineItemCount * buttonWidth + (MaxLineItemCount - 1) * textSpacing;
            float btnLine = Mathf.CeilToInt(len / MaxLineItemCount);
            float minWindowHeight = 300 + (btnLine + 1) * 10;
            // 设置初始窗口位置为屏幕中央
            float x = Screen.currentResolution.width * 0.3f;
            float y = Screen.currentResolution.height * 0.1f;
            position = new Rect(x, y, minWindowWidth, minWindowHeight);
        }
        private void OnGUI()
        {
            if (mCanInit)
            {
                InitializeGUI();
                mCanInit = false;
            }

            EditorGUILayout.Space();

            EditorGUILayout.BeginHorizontal();
            mIsLockBtn = GUILayout.Toggle(mIsLockBtn, "切换功能组");
            mDisabledInputBox = GUILayout.Toggle(mDisabledInputBox, "禁用输入框");
            mShowBaseInfo = GUILayout.Toggle(mShowBaseInfo, "基础信息");
            modes = (T)EditorGUILayout.EnumPopup(modes);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Space();

            EditorGUILayout.BeginHorizontal();
            EditorGUI.BeginDisabledGroup(mDisabledInputBox);
            var op = GUILayout.MaxHeight(18);
            inputText = EditorGUILayout.TextField(inputText, op);
            EditorGUI.EndDisabledGroup();
            mPath = (E_Path)EditorGUILayout.EnumPopup(mPath);
            EditorGUILayout.EndHorizontal();

            if (Event.current.type == EventType.ContextClick)
            {
                // 创建右键菜单
                var menu = new GenericMenu();
                if (mMenuItemFunc != null)
                {
                    for (byte i = 0; i < mMenuItemFunc.Length; i++)
                    {
                        menu.AddItem(mMenuItemContent[i], false, mMenuItemFunc[i]);
                    }
                }
                menu.AddItem(new GUIContent("创建基础目录"), false, () =>
                {
                    string[] fileNames = { "ArtRes", "Editor", "Presets", "Resources", "Scenes", "Framework", "Project", "StreamingAssets" };
                    for (int i = 0; i < fileNames.Length; i++)
                    {
                        string path = Application.dataPath + "/" + fileNames[i];
                        if (!Directory.Exists(path)) Directory.CreateDirectory(path);
                    }
                    AssetDatabase.Refresh();
                });
                // 显示右键菜单
                menu.ShowAsContext();
                Event.current.Use();
            }

            EditorGUILayout.BeginHorizontal();
            if (OnClick("重置状态"))
            {
                mIsLockBtn = true;
                mDisabledInputBox = true;
                mShowBaseInfo = false;
                modes = Empty;
                inputText = "状态已重置!";
            }
            else if (OnClick("打开文件夹"))
            {
                inputText = mPath switch
                {
                    E_Path.DataPath => Application.dataPath,
                    E_Path.PersistentDataPath => Application.persistentDataPath,
                    E_Path.StreamingAssetsPath => Application.streamingAssetsPath,
                    E_Path.TemporaryCachePath => Application.temporaryCachePath,
                    E_Path.DesktopPath => Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
                    E_Path.DocumentsPath => Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                    E_Path.SelectPath => EditorHelper.GetSelectionFolder(),
                    _ => mDisabledInputBox ? "https://gitee.com/PantyNeko" : inputText,
                };
                try
                {
                    if (string.IsNullOrEmpty(inputText))
                    {
                        inputText = "路径为空";
                    }
                    else
                    {
                        System.Diagnostics.Process.Start(inputText);
                    }
                }
                catch (System.ComponentModel.Win32Exception ex)
                {
                    inputText = ex.Message;
                }
            }
            EditorGUILayout.EndHorizontal();

            ShowBtn(mIsLockBtn ? btnInfos : mLockableBtn);

            ExtensionControl();

            if (mShowBaseInfo)
            {
                EditorGUILayout.Space();
                GUILayout.Label($"世界 : {Camera.main.ScreenToWorldPoint(Input.mousePosition)}");
                GUILayout.Label($"视口 : {Camera.main.ScreenToViewportPoint(Input.mousePosition)}");
                GUILayout.Label($"屏幕 : {Input.mousePosition}");
                GUILayout.Label("帧率 : " + (1F / Time.deltaTime).ToString("F0"));
            }
        }
        private void ShowBtn(KeyValuePair<string, Action>[] btns)
        {
            if (btns == null) return;
            int row = btns.Length / MaxLineItemCount;
            if (btns.Length > MaxLineItemCount)
            {
                // 如果按钮的数量大于一行 调整按钮行布局，三行按钮
                for (byte r = 0; r < row; r++)
                {
                    EditorGUILayout.BeginHorizontal();
                    int index = r * MaxLineItemCount;
                    for (byte i = 0; i < MaxLineItemCount; i++)
                    {
                        var pair = btns[i + index];
                        if (OnClick(pair.Key))
                        {
                            pair.Value();
                            break;
                        }
                    }
                    EditorGUILayout.EndHorizontal();
                }
            }
            EditorGUILayout.BeginHorizontal();
            int num = btns.Length % MaxLineItemCount;
            int max = row * MaxLineItemCount;

            for (byte i = 0; i < num; i++)
            {
                var pair = btns[i + max];
                if (OnClick(pair.Key))
                {
                    pair.Value();
                    break;
                }
            }
            EditorGUILayout.EndHorizontal();
        }
        protected virtual void ExtensionControl() { }
        protected virtual KeyValuePair<string, GenericMenu.MenuFunction>[] RightClickMenu() => null;
        protected virtual KeyValuePair<string, Action>[] InitBtnInfo(out KeyValuePair<string, Action>[] btns)
        {
            btns = null;
            return null;
        }
        protected bool OnClick(string name)
        {
            if (GUILayout.Button(name, GUILayout.Height(30)) && Event.current.button == 0)
            {
                Debug.Log($"{name}被点击!");
                return true;
            }
            return false;
        }
    }
}